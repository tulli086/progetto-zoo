package it.begear.zoo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
	@Table(name="animale")
	
	
	public class Animale {
		@Id
		@GeneratedValue(strategy =GenerationType.IDENTITY)
		@Column(name="idanimale")
	private int idanimale;
		@Column(name="specie")
	private String specie;
	    @Column(name="nomeanimale")
	private String nomeanimale;
	    @Column(name="eta")
	private int eta;
	    @Column(name="sesso")
	private String sesso;
	     
	    
		public int getIdanimale() {
			return idanimale;
		}
		public void setIdanimale(int idanimale) {
			this.idanimale = idanimale;
		}
		public String getSpecie() {
			return specie;
		}
		public void setSpecie(String specie) {
			this.specie = specie;
		}
		public String getNomeanimale() {
			return nomeanimale;
		}
		public void setNomeanimale(String nomeanimale) {
			this.nomeanimale = nomeanimale;
		}
		public int getEta() {
			return eta;
		}
		public void setEta(int eta) {
			this.eta = eta;
		}
		public String getSesso() {
			return sesso;
		}
		public void setSesso(String sesso) {
			this.sesso = sesso;
		}
	public Animale(int idanimale, String specie, String nomeanimale, int eta, String sesso, Animale animale) {
	    super();
		this.idanimale = idanimale;
		this.specie = specie;
		this.nomeanimale = nomeanimale;
		this.eta = eta;
		this.sesso = sesso;	
		}
	public Animale() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Animale [idanimale=" + idanimale + ", specie=" + specie + ", nomeanimale=" + nomeanimale + ", eta=" + eta + ", sesso=" + sesso + "]";
		}
}

	
	
	


