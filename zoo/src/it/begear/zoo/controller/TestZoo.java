package it.begear.zoo.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import it.begear.zoo.model.Animale;
import it.begear.zoo.service.AnimaleService;

@Controller
@RequestMapping("/animale")
public class TestZoo {
	
	
	
	
	@Autowired
	AnimaleService animaleservice;
	
	public String listAnimale(Model model) {
	List<Animale> animales = animaleservice.getAnimale();
	
	
	model.addAttribute("animale", animales);
	
	return "list-animale";
	}
	
	
	@GetMapping("/showformAdd")
	public String showformForAdd(Model model) {
		Animale animale = new Animale();
		model.addAttribute("animale", animale);
		return "animale-form";
		
		
}
	@PostMapping("/saveAnimale")
	public String saveAnimale(@ModelAttribute("animale") Animale animale) {
		
		animaleservice.saveAnimale(animale);
		return "redirect:/animale/list";
		
		
	}
	
@GetMapping("/showFormForUpdate")
public String showFormUpdate(@RequestParam("id") int id , Model model) {
	
	//recuperiamo il dato dal database
	
	Animale animale = animaleservice.getAnimale(id);
	
	model.addAttribute("animale", animale);
	
	return "animale-form";
	
	
	
} 

@GetMapping("/delete")
public String deletePersona(@RequestParam("id")int id) {
	
	animaleservice.deleteAnimale(id);
	return "redirect:/animale/list";
	
	
}

@GetMapping("/serch")
public String searchAnimale(@RequestParam("specie")String stringa, Model model) {
	
	
		 List<Animale> listaAnimale = animaleservice.searchAnimale(stringa);
		 model.addAttribute("persona", listaAnimale);
		 
		 return "list-animale";

	
	
	
	
	
}

}

