package it.begear.zoo.service;

import java.util.List;

import it.begear.zoo.model.Animale;



public interface AnimaleService {//comento
	
	
public List<Animale> getAnimale();
	
	public void saveAnimale(Animale animale);
	
	public Animale getAnimale(int id);
	
	public void deleteAnimale(int id);
	
	public List<Animale> searchAnimale(String animale);
	
	
	
	
	
	

}
