package it.begear.zoo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.begear.zoo.dao.DaoAnimale;
import it.begear.zoo.model.Animale;

@Service

public class AnimaleServiceImpl implements AnimaleService {
	
	
	@Autowired
	private DaoAnimale daoanimale;

	@Override
	@Transactional
	public List<Animale> getAnimale() {
		
		return daoanimale.getAnimale() ;
	}

	@Override
	@Transactional
	public void saveAnimale(Animale animale) {
		daoanimale.saveanimale(animale);
	}

	@Override
	@Transactional
	public Animale getAnimale(int id) {
		
		return daoanimale.getAnimale(id);
	}

	@Override
	@Transactional
	public void deleteAnimale(int id) {
	
		daoanimale.deleteAnimale(id);
	}

	@Override
	@Transactional
	public List<Animale> searchAnimale(String animale) {
		
		return daoanimale.searchAnimale(animale);
	}
	
	
}
