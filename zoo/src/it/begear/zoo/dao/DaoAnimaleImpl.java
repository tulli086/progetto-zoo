package it.begear.zoo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import it.begear.zoo.model.Animale;
@Repository
public class DaoAnimaleImpl implements DaoAnimale {
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	
	

	@Override
	public List<Animale> getAnimale() {
		Session session= sessionFactory.getCurrentSession();
		Query<Animale> query = session.createQuery("From Persona", Animale.class );
	 
		List<Animale> animales = query.getResultList();
		return animales;
	}

	@Override
	public void saveanimale(Animale animale) {
	Session session = sessionFactory.getCurrentSession();
		
		session.saveOrUpdate(animale);
		
	}

	@Override
	public Animale getAnimale(int id) {
		
		Session session = sessionFactory.getCurrentSession();
		Animale animale = session.get(Animale.class, id);
		
		return animale;
	}

	@Override
	public void deleteAnimale(int id) {
		Session session = sessionFactory.getCurrentSession();
		Animale animale = session.get(Animale.class, id);
		session.delete(animale);
		
	}

	@Override
	public List<Animale> searchAnimale(String stringa) {
		Session session = sessionFactory.getCurrentSession();
		Query query = null;
		if(stringa != null && stringa.trim().length()>0) {
			
			query=session.createQuery("From Animale where lower(specie) like :specie or lower(nomeanimale) like :nomeanimale", Animale.class);
			query.setParameter("nome", "%" + stringa.toLowerCase() + "%" );
			
		}else {
			
			query = session.createQuery("From Persona", Animale.class);
			
			List<Animale>  animali = query.getResultList();
			return animali;
		}
		return null;
		
		
	}
	}

	
	
	
	
	

