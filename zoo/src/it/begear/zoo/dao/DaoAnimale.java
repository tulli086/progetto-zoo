package it.begear.zoo.dao;

import java.util.List;

import it.begear.zoo.model.Animale;



public interface DaoAnimale {//commento
	
	
	public List<Animale> getAnimale();
	
	public void saveanimale(Animale animale);
	
	public Animale getAnimale(int id);
	
	public void deleteAnimale(int id);
	
	public List<Animale> searchAnimale(String stringa);
	
}
	
	
	


