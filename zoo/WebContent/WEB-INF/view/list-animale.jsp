<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!DOCTYPE html>

<html>

<head>
	<title>List Animales</title>
	
	<!-- reference our style sheet -->

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resource/css/style.css" />

</head>

<body>

	<div id="wrapper">
		<div id="header">
			<h2>Animale CRM</h2>
		</div>
	</div>
	
	<div id="container">
	
		<div id="content">
		
			<!-- put new button: Add Customer -->
		
			<input type="button" value="Add Animale"
				   onclick="window.location.href='showFormForAdd'; return false;"
				   class="add-button"
			/>
			
			
			<input type="button" value="Home"
				   onclick="window.location.href='/zoo/animale/list'; return false;"
				   class="add-button"
			/>
		<form:form action="search" method="GET">
				Search animales: <input type="text" name="nome" />
				
				<input type="submit" value="Search" class="add-button" />
			</form:form>
			<!--  add our html table here -->
		
			<table>
				<tr>
					<th>Specie</th>
					<th>Nome</th>
					<th>Et�</th>
					<th>Sesso</th>
					<th>Action</th>
				</tr>
				
				<!-- loop over and print our customers -->
				<c:forEach var="tempAnimale" items="${animali}">
				
					<!-- construct an "update" link with customer id -->
					<c:url var="updateLink" value="/animale/showFormForUpdate">
						<c:param name="id" value="${tempAnimale.id}" />
					</c:url>	
					
					<!-- construct an "update" link with customer id -->
					<c:url var="deleteLink" value="/animale/delete">
						<c:param name="id" value="${tempAnimale.id}" />
					</c:url>						
					
					<tr>
						<td> ${tempAnimale.specie} </td>
						<td> ${tempAnimale.nomeanimale} </td>
						<td> ${tempAnimale.eta} </td>
							<td> ${tempAnimale.sesso} </td>
						
						<td>
							<!-- display the update link -->
							<a href="${updateLink}">Update</a>
							|
							<a href="${deleteLink}"
							   onclick="if (!(confirm('vuoi eliminare questo animale?'))) return false">Delete</a>

						</td>
						
					</tr>
				
				</c:forEach>
						
			</table>
				
		</div>
	
	</div>
	

</body>

</html>