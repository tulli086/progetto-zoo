<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    
<!DOCTYPE html>
<html>
<head>
	<title>Save Animale</title>

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resource/css/style.css">

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resource/css/add-customer-style.css">
</head>
<body>
	
	<div id="wrapper">
		<div id="header">
			<h2>Animale</h2>
		</div>
	</div>

	<div id="container">
		<h3>Save Animale</h3>
	
		<form:form action="saveAnimale" modelAttribute="animale" method="POST">
		
		<!-- need to associate this data with the customer -->
		<form:hidden path="id" />
		
		
			<table>
				<tbody>
					<tr>
						<td><label>Specie:</label></td>
						<td><form:input path="specie" /></td>
					</tr>
				
					<tr>
						<td><label>Nome:</label></td>
						<td><form:input path="nomeanimale" /></td>
					</tr>

					<tr>
						<td><label>Et�:</label></td>
						<td><form:input path="eta" /></td>
					</tr>
					<tr>
						<td><label>Sesso:</label></td>
						<td><form:input path="sesso" /></td>
					</tr>

					<tr>
						<td><label></label></td>
						<td><input type="submit" value="Save" class="save" /></td>
					</tr>

				
				</tbody>
			</table>
		
		
		</form:form>
	
		<div style="clear; both;"></div>
		
		<p>
			<a href="${pageContext.request.contextPath}/animale/list">Back to List</a>
		</p>
	
	</div>

</body>

</html>